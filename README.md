### Scripts collection for Game Quitters

**A collection of scripts for Windows and Linux to prevent people from gaming**

The focus is on blocking gaming related websites and game clients such as steam, battle.net, NCsoft.

But also providing alternatives by redirecting to specific websites.
