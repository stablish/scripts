# Basic script which assumes that you have installed all the clients to the default location.
# If not, the script will skip blocking that client and jumps to the next


# Steam
$steamPath="C:\Program Files (x86)\Steam\Steam.exe"
If (Test-Path $steamPath){
    Write-Host "Steam is installed, blocking it now!"
    New-NetFirewallRule -DisplayName "Steam_Outbound" -Direction Outbound -Program "C:\Program Files (x86)\Steam\Steam.exe" -Action Block > $null
    New-NetFirewallRule -DisplayName "Steam_Inbound" -Direction Inbound -Program "C:\Program Files (x86)\Steam\Steam.exe" -Action Block > $null
}Else{
  Write-Host "Steam is not installed or not in the default directory...skipping!"
}

# Battle.net
$bnetPath="C:\Program Files (x86)\Battle.net\Battle.net.exe", "C:\Program Files (x86)\Battle.net\Battle.net Launcher.exe"
If (Test-Path $bnetPath){
    Write-Host "Battle.net is installed, blocking it now!"
    New-NetFirewallRule -DisplayName "Battle.net_Launcher_Outbound" -Direction Outbound -Program "C:\Program Files (x86)\Battle.net\Battle.net Launcher.exe" -Action Block > $null
    New-NetFirewallRule -DisplayName "Battle.net_Launcher_Inbound" -Direction Inbound -Program "C:\Program Files (x86)\Battle.net\Battle.net Launcher.exe" -Action Block > $null
    New-NetFirewallRule -DisplayName "Battle.net_Outbound" -Direction Outbound -Program "C:\Program Files (x86)\Battle.net\Battle.net.exe" -Action Block > $null
    New-NetFirewallRule -DisplayName "Battle.net_Inbound" -Direction Inbound -Program "C:\Program Files (x86)\Battle.net\Battle.net.exe" -Action Block > $null

}Else{
  Write-Host "Battle.net is not installed or not in the default directory...skipping!"
}

# NCsoft
$ncsoftPath="C:\Program Files (x86)\NCWest\NCLauncher\NCLauncher.exe"
If (Test-Path $ncsoftPath){
    Write-Host "NCSoft is installed, blocking it now!"
    New-NetFirewallRule -DisplayName "NCSoft_Outbound" -Direction Outbound -Program "$ncsoftPath" -Action Block > $null
    New-NetFirewallRule -DisplayName "NCSoft_Inbound" -Direction Inbound -Program "$ncsoftPath" -Action Block > $null
}Else{
  Write-Host "NCSoft is not installed or not in the default directory...skipping!"
}

# UPlay
$uplayPath="C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\Uplay.exe", "C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\UplayService.exe", "C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\upc.exe"
If (Test-Path $uplayPath){
    Write-Host "UPlay is installed, blocking it now!"
    New-NetFirewallRule -DisplayName "UPlay_Outbound" -Direction Outbound -Program "$uplayPath" -Action Block > $null
    New-NetFirewallRule -DisplayName "UPlay_Inbound" -Direction Inbound -Program "$uplayPath" -Action Block > $null
}Else{
  Write-Host "UPlay is not installed or not in the default directory...skipping!"
}

# League of Legends
$lolPath="C:\Riot Games\League of Legends\LeagueClient.exe", "C:\Riot Games\League of Legends\RADS\projects\league_client\releases\installer\deploy\LeagueClient.exe"
If (Test-Path $lolPath){
    Write-Host "LoL is installed, blocking it now!"
    New-NetFirewallRule -DisplayName "LoL_Outbound" -Direction Outbound -Program "$lolPath" -Action Block > $null
    New-NetFirewallRule -DisplayName "LoL_Inbound" -Direction Inbound -Program "$lolPath" -Action Block > $null
}Else{
  Write-Host "LoL is not installed or not in the default directory...skipping!"
}