@echo off
color 1f
NET SESSION >nul 2>&1
IF %ERRORLEVEL% EQU 0 (
    ECHO ##################################
    ECHO Administrator PRIVILEGES Detected!
    ECHO ##################################
) ELSE (
   echo ######## ########  ########   #######  ########
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ######   ########  ########  ##     ## ########
   echo ##       ##   ##   ##   ##   ##     ## ##   ##
   echo ##       ##    ##  ##    ##  ##     ## ##    ##
   echo ######## ##     ## ##     ##  #######  ##     ##
   echo.
   echo.
   echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
   echo This script must be run as administrator to work properly!
   echo Right click on the file and select "Run as Administrator".
   echo ##########################################################
   echo.
   PAUSE
   EXIT /B 1
)
echo Executing script...
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%~dp0block.ps1""' -Verb RunAs}"