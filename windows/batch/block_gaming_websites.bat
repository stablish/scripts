REM Very basic script to block gaming related websites
REM It will backup your current hosts file
REM And add gaming related websites
REM So you can't access them anymore


NET SESSION >nul 2>&1
IF %ERRORLEVEL% EQU 0 (
    ECHO ##################################
    ECHO Administrator PRIVILEGES Detected!
    ECHO ##################################
) ELSE (
   echo ######## ########  ########   #######  ########
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ######   ########  ########  ##     ## ########
   echo ##       ##   ##   ##   ##   ##     ## ##   ##
   echo ##       ##    ##  ##    ##  ##     ## ##    ##
   echo ######## ##     ## ##     ##  #######  ##     ##
   echo.
   echo.
   echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
   echo This script must be run as administrator to work properly!
   echo Right click on the file and select "Run as Administrator".
   echo ##########################################################
   echo.
   PAUSE
   EXIT /B 1
)

copy C:\Windows\System32\drivers\etc\hosts C:\Windows\System32\drivers\etc\hosts.bak

@echo off
TITLE Modifying your HOSTS file
COLOR 1F
echo.
Set "Hosts=%windir%\System32\drivers\etc\hosts"
Copy "%Hosts%" "%Hosts%.bak"
(
  echo # Steam related
  echo 127.0.0.1 steampowered.com
  echo 127.0.0.1 store.steampowered.com
  echo 127.0.0.1 steamcharts.com
  echo 127.0.0.1 steamcommunity.com
  echo 127.0.0.1 steam.tools
  echo 127.0.0.1 steam.design
  echo 127.0.0.1 steamdb.info
  echo 127.0.0.1 steamspy.com
  echo 127.0.0.1 valvesoftware.com
  echo 127.0.0.1 www.valvesoftware.com
  echo.
  echo # Battle.net / Blizzard related
  echo 127.0.0.1 blizzard.com
  echo 127.0.0.1 www.blizzard.com
  echo 127.0.0.1 battle.net
  echo 127.0.0.1 www.battle.net
  echo 127.0.0.1 worldofwarcraft.com
  echo 127.0.0.1 playhearthstone.com
  echo 127.0.0.1 starcraft.com
  echo 127.0.0.1 starcraft2.com
  echo.
  echo # League of Legends related
  echo 127.0.0.1 riotgames.com
  echo 127.0.0.1 www.riotgames.com
  echo 127.0.0.1 leagueoflegends.com
  echo 127.0.0.1 www.leagueoflegends.com
  echo 127.0.0.1 lolesports.com
  echo 127.0.0.1 www.lolesports.com
  echo 127.0.0.1 lolalytics.com
  echo 127.0.0.1 lolmath.net
  echo 127.0.0.1 lolking.net
  echo 127.0.0.1 www.lolking.net
  echo 127.0.0.1 wol.gg
  echo 127.0.0.1 lolcounter.com
  echo 127.0.0.1 www.lolcounter.com
  echo.
  echo # NCSoft related
  echo 127.0.0.1 ncsoft.com
  echo 127.0.0.1 www.ncsoft.com
  echo 127.0.0.1 bladeandsoul.com
  echo 127.0.0.1 www.bladeandsoul.com
  echo 127.0.0.1 aiononline.com
  echo 127.0.0.1 www.aiononline.com
  echo.
  echo # UPlay related
  echo 127.0.0.1 ubi.com
  echo 127.0.0.1 uplay.ubi.com
  echo 127.0.0.1 www.uplay.ubi.com
  echo 127.0.0.1 store.ubi.com
  echo 127.0.0.1 account.ubisoft.com
  echo 127.0.0.1 www.ubisoft.com
  echo.
  echo # Misc
  echo 127.0.0.1 www.humblebundle.com
  echo 127.0.0.1 humblebundle.com
) > "%Hosts%"


REM Remove the lines below if you dont to block twitch
(
echo.
echo 127.0.0.1 twitch.tv
echo 127.0.0.1 www.twitch.tv
) >> "%Hosts%"
REM Remove the lines above if you dont to block twitch

REM Restarting network adapters
ipconfig /release
ipconfig /flushdns
ipconfig /renew