### Batch (`.bat`) scripts collection

- `block_gaming_websites.bat` - Creates a backup and modifies the Windows hosts file (`C:\System32\drivers\etc\hosts`) so it will block gaming related websites including steam, battle.net/blizzard, twitch, humblebundle and more.

- `revert_block_gaming_websites.bat` - Reverts the changes and creates a empty hosts file

- `block_clients/block.bat` - Will execute the actual script with bypassing the Powershell execution policy

- `block_clients/block.ps1` - Actual script. Will add Firewall rules for specific game clients (Steam, Battle.net, NCSoft, etc.) and blocks the In-/Outbound connections 
