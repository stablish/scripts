REM This script creates a empty hosts file
REM So you can access all websites again


NET SESSION >nul 2>&1
IF %ERRORLEVEL% EQU 0 (
    ECHO ##################################
    ECHO Administrator PRIVILEGES Detected!
    ECHO ##################################
) ELSE (
   echo ######## ########  ########   #######  ########
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ##       ##     ## ##     ## ##     ## ##     ##
   echo ######   ########  ########  ##     ## ########
   echo ##       ##   ##   ##   ##   ##     ## ##   ##
   echo ##       ##    ##  ##    ##  ##     ## ##    ##
   echo ######## ##     ## ##     ##  #######  ##     ##
   echo.
   echo.
   echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
   echo This script must be run as administrator to work properly!
   echo Right click on the file and select "Run as Administrator".
   echo ##########################################################
   echo.
   PAUSE
   EXIT /B 1
)


@echo off
TITLE Modifying your HOSTS file
COLOR 1F
echo.
Set "Hosts=%windir%\System32\drivers\etc\hosts"
Copy "%Hosts%" "%Hosts%.bak"
(
  echo.
) > "%Hosts%"


REM Restarting network adapters
ipconfig /release
ipconfig /flushdns
ipconfig /renew